/**
 * Copyright 2013 - Edward A. Webb
 *
 * This file is par of Confluence Scribbles
 * HTML5 based editor to insert scribbles and drawings in Confluence pages
 * 
 * Confluence Scribbles is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 * Confluence Scribbles is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Confluence Scribbles.  If not, see <http://www.gnu.org/licenses/>.
 */
package ut.com.edwardawebb.confluence.scribbles;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.containsString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.edwardawebb.confluence.scribbles.ScribbleDoesNotExistException;
import com.edwardawebb.confluence.scribbles.ScribbleService;
import com.edwardawebb.confluence.scribbles.ScribbleServiceBandanaImpl;
import com.edwardawebb.confluence.scribbles.ScribbleSvg;

/**
 * @author Edward A. Webb (http://edwardawebb.com)
 * 
 */
public class BandanaPersistenceTests {
	private static Logger LOG = LoggerFactory
			.getLogger(ScribbleServiceBandanaImpl.class);
	
	private static final String SCRIBBLE_ONE_CONTENT= "<svg title=\"foofoo\"/>";

	private static final String SCRIBBLE_TWO_CONTENT = "<svg title=\"newnew\"";
	
	private AttachmentManager attachmentManager;  
	private ScribbleService scribbleService; 
	private ContentEntityObject pageOne;
	private long currentAttachmentId=1000L;
	Attachment pageOneAttachmentOne ; //used across tests for uppdates
	Attachment pageOneAttachmentTwo ; //used across tests for uppdates

	
	
	@Before
	public void setUp() throws Exception {
		 pageOne = mock(ContentEntityObject.class);
		 pageOneAttachmentOne = getNewAttachment("scribbledAttachment",pageOne);
		 pageOneAttachmentTwo = getNewAttachment("nonScribbledAttachment",pageOne);
		
		attachmentManager = mock(AttachmentManager.class);
		when(attachmentManager.getAttachment(pageOne, "scribbledAttachment")).thenReturn(pageOneAttachmentOne);
		when(attachmentManager.getAttachment(pageOne, "nonScribbledAttachment")).thenReturn(pageOneAttachmentTwo);
		//when(attachmentManager.getAttachment(pageOne, anyString())).thenReturn(attachment);
		//when(attachmentManager.getAttachment(pageOne, anyString())).thenReturn(attachment);
		
		
		
		
		BandanaManager bandanaManager = mock(BandanaManager.class);
		
		
		scribbleService = new ScribbleServiceBandanaImpl(attachmentManager,
				bandanaManager); 
		
		
		//create the initial scribble for attachment one (something previuosly scribbled)
		ScribbleSvg existingScribble = scribbleService.loadOrCreateSvgDefinitionForAttachment(pageOne, pageOneAttachmentOne.getFileName());
		existingScribble.setAttachmentId(pageOneAttachmentOne.getId());
		existingScribble.setAttachmentVersion(pageOneAttachmentOne.getVersion());
		existingScribble.setDefinition(SCRIBBLE_ONE_CONTENT);
		scribbleService.updateSvgDefinition(SCRIBBLE_ONE_CONTENT, pageOneAttachmentOne);
		LOG.debug("persisted {}",existingScribble);
	}


	@Test
	@Ignore("Switching from AO, need to wire in container for these test")
	public void aScribbleCanBeCreatedForAnUnScribbledAttachment() throws IOException{
		//when the user tries to edit an image attachment for the first time, 
		ScribbleSvg scribbleSvg =scribbleService.loadOrCreateSvgDefinitionForAttachment(pageOne, "nonScribbledAttachment");
		
		//then a new scribble is created
		assertThat(scribbleSvg,notNullValue());
		assertThat(scribbleSvg.getDefinition(),containsString("<image "));
	};
	
	@Test
	@Ignore("Switching from AO, need to wire in container for these test")
	public void aNewScribbleCanBeUpdated() throws IOException, ScribbleDoesNotExistException{
		LOG.debug("saving new scriblle, expect version 0");
		//when the user creates a new scribble
		ScribbleSvg scribbleSvg =scribbleService.loadOrCreateSvgDefinitionForAttachment(pageOne, "nonScribbledAttachment");
		LOG.debug("persisted ScribbleSvg attachment ID {}",scribbleSvg.getAttachmentId());
		LOG.debug("attachment ID {}",pageOneAttachmentTwo.getId());
		assertThat(scribbleSvg,notNullValue());
	
		
		
		//and tries to save the edits	
		when(pageOneAttachmentTwo.getVersion()).thenReturn(2);
		scribbleService.updateSvgDefinition(SCRIBBLE_TWO_CONTENT,pageOneAttachmentTwo);
		
		
		//then the scribble and attachment are associated, and the scribble ID remains constant.
		assertThat(scribbleSvg.getAttachmentId(),is(pageOneAttachmentTwo.getId()));
		assertThat(scribbleSvg.getAttachmentVersion(),is(pageOneAttachmentTwo.getVersion()));
		assertThat(scribbleSvg.getDefinition(),is(SCRIBBLE_TWO_CONTENT));
	};
	
	@Test
	@Ignore("Switching from AO, need to wire in container for these test")
	public void anExistingScribbleCanBeFound() throws IOException{
		ScribbleSvg scribbleSvg =scribbleService.loadOrCreateSvgDefinitionForAttachment(pageOne, "scribbledAttachment");
		assertThat(scribbleSvg,notNullValue());
		assertThat(scribbleSvg.getDefinition(),is(SCRIBBLE_ONE_CONTENT));
		
	};
	

	@Test
	@Ignore("Switching from AO, need to wire in container for these test")
	public void anExistingScribbleCanBeUpdatedProvidedTheAttachmentIsVersioned() throws IOException, ScribbleDoesNotExistException{
		LOG.debug("anExistingScribbleCanBeUpdatedProvidedTheAttachmentIsVersioned>>>");
		//when the user edits an attachment AND a scribble already exists for that version
		ScribbleSvg scribbleSvg =scribbleService.loadOrCreateSvgDefinitionForAttachment(pageOne, "scribbledAttachment");
		LOG.debug("Loaded ScribbleSvg {}, att version {}",scribbleSvg,scribbleSvg.getAttachmentVersion());
		LOG.debug("and ID {}",scribbleSvg.getAttachmentId());
		assertThat(scribbleSvg,notNullValue());
		
		// mimic the act of saving to attachment manager which will increment version
		when(pageOneAttachmentOne.getVersion()).thenReturn(2);
		LOG.debug("Atttempt to save with attachment {}, att version {}",pageOneAttachmentOne.getId(),pageOneAttachmentOne.getVersion());
		
		
		scribbleService.updateSvgDefinition(scribbleSvg.getDefinition(),pageOneAttachmentOne);
		
		assertThat(scribbleSvg.getDefinition(),is(SCRIBBLE_ONE_CONTENT));
		assertThat(scribbleSvg.getAttachmentId(),is(pageOneAttachmentOne.getId()));
		assertThat(scribbleSvg.getAttachmentVersion(),is(pageOneAttachmentOne.getVersion()));
		LOG.debug("anExistingScribbleCanBeUpdatedProvidedTheAttachmentIsVersioned<<<");
		
	};
	

	@Test(expected=IllegalStateException.class)
	@Ignore("Switching from AO, need to wire in container for these test")
	public void updatingAnExistingScribbleAndAttachmentRequiresNewAttachmentVersion() throws IOException, ScribbleDoesNotExistException{
		LOG.debug("updatingAnExistingScribbleAndAttachmentRequiresNewAttachmentVersion>>>");
		//when the user edits an attachment AND a scribble already exists for that version
		ScribbleSvg scribbleSvg =scribbleService.loadOrCreateSvgDefinitionForAttachment(pageOne, "scribbledAttachment");
		LOG.debug("Loaded ScribbleSvg {}, att version {}",scribbleSvg,scribbleSvg.getAttachmentVersion());
		LOG.debug("and ID {}",scribbleSvg.getAttachmentId());
		LOG.debug("Atttempt to save with attachment {}, att version {}",pageOneAttachmentOne.getId(),pageOneAttachmentOne.getVersion());
		assertThat(scribbleSvg,notNullValue());
		
		scribbleService.updateSvgDefinition(scribbleSvg.getDefinition(),pageOneAttachmentOne);
		
		assertThat(scribbleSvg.getDefinition(),is(SCRIBBLE_ONE_CONTENT));
		assertThat(scribbleSvg.getAttachmentId(),is(pageOneAttachmentOne.getId()));
		assertThat(scribbleSvg.getAttachmentVersion(),is(pageOneAttachmentOne.getVersion()));
		LOG.debug("updatingAnExistingScribbleAndAttachmentRequiresNewAttachmentVersion<<<");
		
	}
	
	/**
	 * Still not sure this is how we want to act, but if a user uploads an attachment outside of Scribbles
	 * then any previous {@link ScribbleSvg} becomes irrelevant.
	 * 
	 * The only reason to leave old versions is should the user also remove a newer version, reverting back to previous version.
	 * @throws IOException 
	 */
	@Test
	@Ignore("Switching from AO, need to wire in container for these test")
	public void whenAScribbleIsRequestedForAnOutdatedVersionThePreviousIsPurgedAndNewCreated() throws IOException{
		LOG.debug("whenAScribbleIsRequestedForAnOutdatedVersionThePreviousIsPurged>>>");
		// there exists a pre-persisted scribble @ version 1 (see #setup)
		ScribbleSvg scribble = scribbleService.loadOrCreateSvgDefinitionForAttachment(pageOne, pageOneAttachmentOne.getFileName());
		long attachmentId=scribble.getAttachmentId();
		int attachmentVersion=scribble.getAttachmentVersion();
		assertThat(attachmentVersion,is(1));
		
		//modify attachment outside of scribbles (same ID, new version)
		when(pageOneAttachmentOne.getVersion()).thenReturn(2);
		
		
		//attempt to load scribble from attachment
		scribbleService.loadOrCreateSvgDefinitionForAttachment(pageOne, "scribbledAttachment");
		
		
		ScribbleSvg cleanedUpScribbles = scribbleService.loadOrCreateSvgDefinitionForAttachment(pageOne, pageOneAttachmentOne.getFileName());
		assertThat(cleanedUpScribbles.getAttachmentId(),is(attachmentId));
		assertThat(cleanedUpScribbles.getAttachmentVersion(),not(attachmentVersion));
		LOG.debug("whenAScribbleIsRequestedForAnOutdatedVersionThePreviousIsPurged<<<");		
	}
	

	private InputStream getTestImageInputStream() throws FileNotFoundException {
		 InputStream is = this.getClass().getResourceAsStream("/images/2downarrow32.png");
		 return is;
	}
	
	/**
	 * returns version 1 of a unique ID attachment with a valid content stream
	 * @return
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	private Attachment getNewAttachment(String filename,ContentEntityObject page) throws FileNotFoundException, IOException{
		Attachment newAttachment = mock(Attachment.class);
		when(newAttachment.getContent()).thenReturn(page);
		when(newAttachment.getFileName()).thenReturn(filename);
		when(newAttachment.getVersion()).thenReturn(1);
		when(newAttachment.getId()).thenReturn(currentAttachmentId++);
		when(newAttachment.getContentsAsStream()).thenReturn(getTestImageInputStream());
		return newAttachment;
	}
	
}
