/**
 * Copyright 2013 - Edward A. Webb
 *
 * This file is par of Confluence Scribbles
 * HTML5 based editor to insert scribbles and drawings in Confluence pages
 * 
 * Confluence Scribbles is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 * Confluence Scribbles is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Confluence Scribbles.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.edwardawebb.confluence.scribbles;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.StringTokenizer;

import javax.imageio.ImageIO;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.pages.Attachment;
import com.edwardawebb.confluence.scribbles.actions.ScribbleSaveAction;

/**
 * TODO rename or refactor this class, what an awful and unclear thing...
 * @author Edward A. Webb (http://edwardawebb.com)
 *
 */
public class ImageAttachmentCreator {
	private static Logger LOG = LoggerFactory.getLogger(ScribbleSaveAction.class);
	
	/**
	 * Enabled quick generation of an embedded raster image SVG payload
	 * Argumnents: width, height, title, imageElement
	 */
	public static String SVG_XML = "<svg width=\"%s\" height=\"%s\" xmlns:xlink=\"http://www.w3.org/1999/xlink\" xmlns=\"http://www.w3.org/2000/svg\"><g><title>%s</title>%s</g></svg>";
	/**
	 * enables quick generation of image element to pass to {@link #SVG_XML}
	 * arguments: width, height, base64Uri
	 */
	public static String IMAGE_XML = "<image id=\"svg_2\" width=\"%s\" height=\"%s\" y=\"0\" x=\"0\" xlink:href=\"%s\" />";
	public static String DATA_PREFIX ="data:";
	public static String BASE_64 =";base64";
	
	
	
	/**
	 * Will accept a string represented an encoded image and return decoded byte array
	 * @param annotatedValue the full "data:image/png;base64,iVBORw0KGgo..." or data only "iVBORw0KGgo..." value
	 * @return
	 * @throws IOException
	 */
	 public static byte[] dataFromEncodedBase64PngString(String annotatedValue) throws IOException{
	    	//LOG.warn("Processing base64 string {}",annotatedValue);
		 	StringTokenizer tokenizer = new StringTokenizer(annotatedValue,",");
		 	String dataUri=null;
		    while(tokenizer.hasMoreTokens()){
			 	dataUri = tokenizer.nextToken();
		    }
		    byte[] imgBytes = Base64.decodeBase64(dataUri); 
	    	
	    	return imgBytes;
	    	
	    }
	

		/**
		 * Used to create new scribbles
		 * @return
		 */
		public static String newEmptyDefinition() {
			String svgString = String.format(SVG_XML, 600, 400,"My New ScribbleSvg","" );
			return svgString;
		}

	 
		/**
		 * Creates a SVG xml defition from confluence {@link Attachment} object.
		 * @param attachment
		 * @return
		 * @throws IOException
		 */
		public static  String svgDefinitionFromImageAttachment(Attachment attachment) throws IOException{
			byte[] bytes = IOUtils.toByteArray(attachment.getContentsAsStream());
			BufferedImage image = ImageIO.read(new ByteArrayInputStream(bytes));
			
			byte[] base64 = Base64.encodeBase64(bytes);
			
			StringBuilder dataUri = new StringBuilder();
			dataUri.append(DATA_PREFIX);
			dataUri.append(attachment.getContentType());
			dataUri.append(BASE_64);
			dataUri.append(",");
			dataUri.append(new String(base64,0,base64.length-1));
			
			return fromBase64ImageString(dataUri.toString(), image.getWidth(),image.getHeight(),attachment.getDisplayTitle());
			
		}
		/**
		 * Used on first time editing an existing non-scribble image attachment
		 * @param base64Uri
		 * @param width
		 * @param height
		 * @param title
		 * @return
		 */
		private static String fromBase64ImageString(String base64Uri,int width, int height,String title) {
			String imageElement = String.format(IMAGE_XML, width, height, base64Uri);
			String svgString = String.format(SVG_XML, width, height,title,imageElement );
			return svgString;
		}
}
