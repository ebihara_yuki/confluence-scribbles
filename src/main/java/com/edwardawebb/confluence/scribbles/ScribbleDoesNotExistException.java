/**
 * Copyright 2013 - Edward A. Webb
 *
 * This file is par of Confluence Scribbles
 * HTML5 based editor to insert scribbles and drawings in Confluence pages
 * 
 * Confluence Scribbles is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 * Confluence Scribbles is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Confluence Scribbles.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.edwardawebb.confluence.scribbles;

/**
 * This exception may be thrown when attempting calls to {@link ScribbleService#updateSvgDefinition(String, com.atlassian.confluence.pages.Attachment)} 
 * which does not yet have an @{link ScribbleSvg}. 
 *  When caught the calling service should invoke {@link ScribbleService#saveNewDefinition(String, com.atlassian.confluence.pages.Attachment)} instead.
 * 
 * <pre>try{
			scribbleService.updateSvgDefinition(imageSvg, currentAttachment);
		}catch(ScribbleDoesNotExistException sdne){
			scribbleService.saveNewDefinition(imageSvg, currentAttachment);
		}</pre>
 * 
 * @author Edward A. Webb (http://edwardawebb.com)
 *
 */
public class ScribbleDoesNotExistException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8451777936611562872L;

}
