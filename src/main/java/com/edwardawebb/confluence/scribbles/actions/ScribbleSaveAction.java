/**
 * Copyright 2013 - Edward A. Webb
 *
 * This file is par of Confluence Scribbles
 * HTML5 based editor to insert scribbles and drawings in Confluence pages
 * 
 * Confluence Scribbles is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 * Confluence Scribbles is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Confluence Scribbles.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.edwardawebb.confluence.scribbles.actions;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.core.Versioned;
import com.atlassian.confluence.pages.AbstractPage;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.actions.PageAware;
import com.edwardawebb.confluence.scribbles.ImageAttachmentCreator;
import com.edwardawebb.confluence.scribbles.ScribbleDoesNotExistException;
import com.edwardawebb.confluence.scribbles.ScribbleService;

/**
 * @author Edward A. Webb (http://edwardawebb.com)
 *
 */
public class ScribbleSaveAction extends ConfluenceActionSupport implements PageAware {
	private static Logger LOG = LoggerFactory.getLogger(ScribbleSaveAction.class);
	
	private static final String SCRIBBLE_NAME_PREFIX = "ScribbleSvg-";
	private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss");
	private AbstractPage page;
	private String imageBinary;
	private String imageSvg;


	private boolean existing =false;	
	private String attachmentName;


	private AttachmentManager attachmentManager;
	private ScribbleService scribbleService;
	
	public ScribbleSaveAction(AttachmentManager attachmentManager,ScribbleService scribbleService) {
		this.attachmentManager = attachmentManager;
		this.scribbleService = scribbleService;
	}
	
	
	@Override
	public String execute() throws Exception {
		LOG.debug("Recieved request to save ScribbleSvg for page {}",page.getId());
		if(null == imageBinary || null == imageSvg || null == page ){
			return ERROR;
		}
		//LOG.warn("ImageBinary: {}",imageBinary);
		//LOG.warn("ImageSvg: {}",imageSvg);
		validateOrCreateAttachmentName();		
		persistAttachmentAndScribble();
		insertNewImagesAsMacro();
		return SUCCESS;
      }



	private void validateOrCreateAttachmentName() throws  UnsupportedEncodingException {
		if(! StringUtils.isEmpty(attachmentName)){
			if(!attachmentName.endsWith(".png")){
				attachmentName+=".png";
			}
		}else{
			attachmentName = SCRIBBLE_NAME_PREFIX + dateFormat.format(new Date()) + ".png";
		}
		//attachmentName.replaceAll("/", "-slash-");
		//attachmentName = URLEncoder.encode(attachmentName,"UTF-8");
	}

	

	
	/**
	 * See {@link Versioned} for details. 
	 * The general gist is to not create a new from previous, but demoting the current TO previous,
	 * then editing current.
	 *   <pre>Versioned oldVersion = (Versioned) currentObject.clone();
	 *	 oldVersion.convertToHistoricalVersion();
     *	 oldVersion.setOriginalVersion(currentObject);
	 *   // make changes to currentObject	 *  
	 *	 // DESPITE LINKED DOCS - DONT DUE THIS! --> currentVersion.setVersion(currentVersion.getVersion() + 1);
	 *   // THe result is a {@link StaleObjectStateException}
	 *	 // save both objects</pre>
	 * @throws IOException
	 * @throws CloneNotSupportedException
	 */
	private void persistAttachmentAndScribble() throws IOException, CloneNotSupportedException {
		byte[] image = ImageAttachmentCreator.dataFromEncodedBase64PngString(imageBinary);
		Attachment previousAttachment=null;
		Attachment currentAttachment=null;
		if(existing){
			currentAttachment = attachmentManager.getAttachment(page, attachmentName);
		}
		if(null != currentAttachment){
			LOG.warn("loaded version {} of existing attachment {}",currentAttachment.getVersion(),attachmentName);
			//create historical from current
			previousAttachment = (Attachment) currentAttachment.clone();
			previousAttachment.convertToHistoricalVersion();
			previousAttachment.setOriginalVersion(currentAttachment);
			
			//modify current version
			//currentAttachment.setVersion(currentAttachment.getVersion()+1);
			currentAttachment.setFileSize(image.length);
			currentAttachment.setLastModificationDate(currentAttachment.getCurrentDate());
		} else {
			currentAttachment = new Attachment(attachmentName,"image/png",image.length, null);
			currentAttachment.setContent(page);
		}

		attachmentManager.saveAttachment(currentAttachment, previousAttachment, new ByteArrayInputStream(image));
		LOG.warn("Submitted filename {}",attachmentName);
		LOG.warn("saved attachment {}, version {}",currentAttachment.getId(),currentAttachment.getVersion());
		LOG.warn("filename {}, displayTitle {}",currentAttachment.getFileName(),currentAttachment.getDisplayTitle());
		try{
			scribbleService.updateSvgDefinition(imageSvg, currentAttachment);
		}catch(ScribbleDoesNotExistException sdne){
			scribbleService.saveNewDefinition(imageSvg, currentAttachment);
		}
	}

	private void insertNewImagesAsMacro() {
		if (!existing) {
			StringBuilder currentContent = new StringBuilder(page.getContentEntityObject().getBodyAsString());
			currentContent.append("\n\n");
			currentContent.append("<ac:image>");
			currentContent.append("<ri:attachment ri:filename=\"");
			currentContent.append(attachmentName);
			currentContent.append("\" />");
			currentContent.append("</ac:image>");
			page.getContentEntityObject().setBodyAsString(currentContent.toString());
		}
	}
	
	@Override
	public AbstractPage getPage() {
		return page;
	}

	@Override
	public boolean isLatestVersionRequired() {
		return true;
	}

	@Override
	public boolean isPageRequired() {
		return true;
	}

	@Override
	public boolean isViewPermissionRequired() {
		return true;
	}

	@Override
	public void setPage(AbstractPage page) {
		this.page = page;
	}

	public String getImageBinary() {
		return imageBinary;
	}

	public void setImageBinary(String imageBinary) {
		this.imageBinary = imageBinary;
	}

	public boolean isExisting() {
		return existing;
	}

	public void setExisting(boolean existing) {
		this.existing = existing;
	}


	

	public String getAttachmentName() {
		return attachmentName;
	}

	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}

	



	public String getImageSvg() {
		return imageSvg;
	}



	public void setImageSvg(String imageSvg) {
		this.imageSvg = imageSvg;
	}


	
	
}
