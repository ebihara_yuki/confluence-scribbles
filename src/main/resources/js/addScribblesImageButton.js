AJS.toInit(function($){
	 
	var existingImageButtons = AJS.Confluence.PropertyPanel.Image.pluginButtons;
	
	existingImageButtons.push(null);
	existingImageButtons.push({create: function($img){
        return {
           click: function (a, img) {            	
                      
            	//console.log(img);
            	var attachmentName = img.getAttribute("data-linked-resource-default-alias");
            	var editScribbleUrl = AJS.Data.get("context-path") + "/plugins/scribbles/scribbleedit-svg.action?pageId=" + AJS.params.pageId + "&attachmentName=" + attachmentName;
            	window.location = editScribbleUrl ;
            	//$img.data-linked-resource-default-alias
            	//console.log("sending to:"+editScribbleUrl);
            	
               
            },
            //style button like others
            className: "image-effects scribbles",
            text: AJS.I18n.getText("scribbles.edit.image.button"),
            tooltip: AJS.I18n.getText("scribbles.edit.image.button.tooltip"),
            //only allow scribbles on embedded attachments
            disabled: $img.hasClass('confluence-external-resource'),
            selected: false
        }
    }});
    
}); //end AJS init.