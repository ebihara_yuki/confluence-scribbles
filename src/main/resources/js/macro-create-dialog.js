AJS.bind("init.rte", function (event) {
    (function ($) {
		
		//http://docs.atlassian.com/aui/latest/sandbox/#
		var createScribbleMacro = function (data){
			
			//grab their current place in page
			AJS.Rte.BookmarkManager.storeBookmark();

			var dialog = new AJS.Dialog({
			    width:970, 
			    height:590, 
			    id:"scribble-macro-dialog", 
			    closeOnOutsideClick: true
			});
			dialog.addHeader("Create New Scribble","scribble-header");
			dialog.addCancel(AJS.I18n.getText("scribbles.button.cancel"), function (dialog) {
				AJS.Rte.BookmarkManager.restoreBookmark();
				dialog.hide();				
			});
			
			//second argument is a jQuery selector, soy template, OR textual content, but since there is no content in the current page to use, i think we need soy
			// http://www.slideshare.net/wseliga/better-frontend-development-in-atlassian-plugins
			// http://www.slideshare.net/wseliga/better-frontend-development-in-atlassian-plugins
			
			dialog.addPanel(
				AJS.I18n.getText("scribbles.macro.help"),
				scribbles.soy.content.macroHelp(),
				"scribble-panel-body"
			);
			dialog.addPanel(
				AJS.I18n.getText("scribbles.macro.create"),
				scribbles.soy.content.macroPanel({pageId: AJS.params.pageId}),
				"scribble-panel-body"
			);
			
			dialog.show();
			return false;
		}
		
		//See https://developer.atlassian.com/display/AUI/Dialog
		// and https://answers.atlassian.com/questions/143669/create-custom-macro-dialog-in-confluence
		// and http://www.slideshare.net/GoAtlassian/pimp-my-confluence-plugin-atlascamp-2011 (slide 135)
		AJS.MacroBrowser.setMacroJsOverride('scribble', {opener:createScribbleMacro});

		
    }(AJS.$));

});